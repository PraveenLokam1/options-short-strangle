# options-short-strangle

# option_watcher

### 5 Golden Rules

Rules formed by Daytrader telugu youtube channel.
https://www.youtube.com/channel/UCeEP6GESh2t4-e2132uj3Sg


1. Select 115, 115 premium and SELL PUT, CALL and then select nearly 10% options and BUY. Go to max 20% if 10% not possible

2. Enter on Thursday for the first time and for exit wait for 80% premium decay or 4000 profit or Thursday morning which ever is happening earlier. 
   if any of the above event happened before Thursday, exit from options and start strategy with next week options

   Say if event happened on Monday, exit current and enter next week right away.

3. Start adjusting if lower premium < 50% of high premium

   start adjusting with 80 to 95% of large premium to replace small premium one

4. Try to reduce the difference of 2 premiums at the end of daily trading session. For day end don't look for 50% rule like above.

   For day end try to maintain only <=20%  difference between premiums

5. When strangle becomes straddle, add 2 premiums and then keep 10% in that as stop loss. If it hits stop loss....accept loss for that trade if any and exit as soon as possible.



