import requests
import pandas as pd
from option_config import url
from option_config import CONFIG, ALL_OPTION_DATA_PATH, HEADERS, OPTION_DATA_EXTRACTION_INTERVAL
from util import write_option_data_to_csv
import time
import logging
import sys

logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s - %(levelname)s - %(funcName)s - %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S'
)
logger = logging.getLogger(__name__)
logger.addHandler((logging.StreamHandler(sys.stdout)))
logger.propagate = False
logger.setLevel(logging.INFO)

expiry_date = CONFIG['expiry_date']

pd.set_option('display.max_columns', None)


def get_option_data():
    json_obj = requests.get(url, headers = HEADERS).json()
    print(f"Total option data count received count: {len(json_obj['records']['data'])}")

    temp_data = [option_val for option_val in json_obj['records']['data'] if option_val['expiryDate'] == expiry_date]
    next_week_data = [{**option_val['CE'], **{'optionType': 'CE'}} for option_val in temp_data if 'CE' in option_val] + \
                     [{**option_val['PE'], **{'optionType': 'PE'}} for option_val in temp_data if 'PE' in option_val]
    print(f"Extracted count of CALL and PUT options data extracted for this week {expiry_date} : {len(next_week_data)}")

    df = pd.DataFrame(next_week_data)[['optionType', 'underlyingValue', 'strikePrice', 'lastPrice']]
    return df

def extract_option_data():
    df = None
    while True:
        try:
            df = get_option_data()
            write_option_data_to_csv(df, ALL_OPTION_DATA_PATH)
            logger.info(f'Sleeping for {OPTION_DATA_EXTRACTION_INTERVAL/60} minutes')
            time.sleep(OPTION_DATA_EXTRACTION_INTERVAL)
        except Exception as e:
            logger.error(e)
            logger.info(f'Option data extraction from NSE site failed...trying after {round(OPTION_DATA_EXTRACTION_INTERVAL/180)} minutes')
            time.sleep(round(OPTION_DATA_EXTRACTION_INTERVAL))

if __name__ == "__main__":
    extract_option_data()