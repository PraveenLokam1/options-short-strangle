import pandas as pd
from config.option_config import ALL_OPTION_DATA_PATH, SELECTED_WEEK_OPTIONS_PATH, \
     PREMIUM_FALL_PERCENT, ADJUSTMENT_RANGE

def write_option_data_to_csv(df: pd.DataFrame, path, header: bool = True, mode: str = 'w',):
    print(f'Writing data to CSV in path : {path}')
    df.to_csv(path, index=False, header=header, mode=mode)

def read_input_df(path):
    return pd.read_csv(path)

def mobile_alert():
    print('Alert SMS sending...')


def selected_week_options(return_data: str= 'ALL'):
    selected_options_df = read_input_df(SELECTED_WEEK_OPTIONS_PATH)
    selected_options_dict = selected_options_df.set_index('optionType').T.to_dict('dict')
    call_strike_price = selected_options_dict['CE']['strikePrice']
    put_strike_price = selected_options_dict['PE']['strikePrice']

    if return_data == 'PRICES':
        return call_strike_price, put_strike_price
    elif return_data == 'DF':
        return selected_options_df
    elif return_data == 'DICT':
        return selected_options_dict
    else:
        return selected_options_df, selected_options_dict, call_strike_price, put_strike_price

def current_option_data(return_data: str= 'ALL'):
    df = read_input_df(ALL_OPTION_DATA_PATH)
    call_strike_price, put_strike_price = selected_week_options('PRICES')
    current_df = df.loc[((df.optionType == "CE") & (df.strikePrice == call_strike_price)) |
                        (df.optionType == "PE") & (df.strikePrice == put_strike_price)]
    current_options_dict = current_df.set_index('optionType').T.to_dict('dict')
    call_strike_price = current_options_dict['CE']['strikePrice']
    put_strike_price = current_options_dict['PE']['strikePrice']

    if return_data == 'PRICES':
        return call_strike_price, put_strike_price
    elif return_data == 'DF':
        return current_df
    elif return_data == 'DICT':
        return current_options_dict
    else:
        return current_df, current_options_dict, call_strike_price, put_strike_price

def check_premium_drop(selected_options_df: pd.DataFrame, current_df: pd.DataFrame, only_sell:bool = True):
    print('Calculating current premium...')
    if only_sell:
        selected_premium = selected_options_df[selected_options_df['optionType'] == 'CE']['lastPrice'].iloc[0]  + \
                           selected_options_df[selected_options_df['optionType'] == 'PE']['lastPrice'].iloc[0]

        print(f'Selected premium : {selected_premium}')

        current_premium = current_df[current_df['optionType'] == 'CE']['lastPrice'].iloc[0]  + \
                          current_df[current_df['optionType'] == 'PE']['lastPrice'].iloc[0]

        print(f'Current premium : {current_premium}')

        fall_premium_perc = round((100 - (100 * current_premium) / selected_premium), 2)
        if (PREMIUM_FALL_PERCENT - fall_premium_perc) <= 50:
            print(f'Alerting to exit the options... as current option fall at : {fall_premium_perc}')
            mobile_alert()
        else:
            print(f'Wait for some time as current option fall at : {fall_premium_perc} %')
    else:
        print('Need logic to calculate premium of BUY and SELL options....')

def find_option(option_type: str, premium_range: float):
    all_df = read_input_df(ALL_OPTION_DATA_PATH)
    # x = (ADJUSTMENT_RANGE[0]/100) * premium_range
    # y = (ADJUSTMENT_RANGE[1]/100) * premium_range
    required_df = all_df[(all_df['optionType'] == option_type) &
                         (all_df['lastPrice'] >= (ADJUSTMENT_RANGE[0]/100) * premium_range) &
                         (all_df['lastPrice'] <= (ADJUSTMENT_RANGE[1]/100) * premium_range)]
    if required_df.empty:
        range = ADJUSTMENT_RANGE[0]-1
        while required_df.empty:
            print(range)
            required_df = all_df[(all_df['optionType'] == option_type) &
                                 (all_df['lastPrice'] >= (range / 100) * premium_range) &
                                 (all_df['lastPrice'] <= (ADJUSTMENT_RANGE[1] / 100) * premium_range)]
            range = range-1
    print(required_df)

if __name__ == "__main__":
    df = current_option_data()
    print(df)