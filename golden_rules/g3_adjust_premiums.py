from util import current_option_data, find_option



def do_adjustments_if_needed():
    current_options_dict = current_option_data('DICT')
    print(current_options_dict)
    call_strike_price, put_strike_price = current_option_data('PRICES')
    max = 'CE' if call_strike_price > put_strike_price else 'PE'
    min = 'CE' if call_strike_price < put_strike_price else 'PE'
    print(max)
    perc_diff = 100 - (current_options_dict[min]['lastPrice'] / current_options_dict[max]['lastPrice']) * 100
    print(f"Percentage difference between min and max option : {perc_diff}")
    find_option(min, current_options_dict[max]['lastPrice'])
    if perc_diff > 50:
        print('adjusting')
    else:
        print('no adjusting')

if __name__ == "__main__":
    do_adjustments_if_needed()