import pandas as pd
from option_config import PREMIUM, ALL_OPTION_DATA_PATH, OPTION_LOG_PATH, \
    SELECTED_WEEK_OPTIONS_PATH, PREMIUM_FALL_PERCENT, ADJUSTMENTS_PATH
from pathlib import Path
from util import write_option_data_to_csv, mobile_alert, read_input_df, \
    selected_week_options, current_option_data


def clean_df(df: pd.DataFrame):
    df = df[((df['strikePrice'] > df['underlyingValue']) & (df['optionType'] == 'CE')) |
            ((df['strikePrice'] < df['underlyingValue']) & (df['optionType'] == 'PE'))]
    return df

def add_diff_col_df(df : pd.DataFrame):
    df.loc[:, 'absolute_difference'] = df['lastPrice'].map(lambda x : abs(x - PREMIUM))
    return df

def final_data_df(df: pd.DataFrame):
    temp_df = None
    for i in range(100):
        temp_df = df[df['absolute_difference'] <= i]
        option_types = set(temp_df['optionType'])
        if option_types == {'PE', 'CE'}:
            print('Both CALL and PUT options available...')
            break
    return temp_df

def select_final_options(df: pd.DataFrame):
    c_df = df[df['optionType'] == 'CE']
    p_df = df[df['optionType'] == 'PE']

    c_df['key'] = 1
    p_df['key'] = 1

    merge_df = pd.merge(c_df, p_df, on='key').drop(["key"], 1)

    merge_df.loc[:, 'diff_from_115'] = merge_df.apply(
        lambda row: abs(row['absolute_difference_x'] + row['absolute_difference_y']), axis=1)
    merge_df.loc[:, 'combo_diff'] = merge_df.apply(lambda row: abs(row['lastPrice_x'] - row['lastPrice_y']), axis=1)
    merge_df.loc[:, 'all_diff'] = merge_df.apply(lambda row: (row['combo_diff'] + row['diff_from_115']), axis=1)

    min_df = merge_df[merge_df.all_diff == merge_df.all_diff.min()]
    if len(min_df.index) > 1:
        min_df = min_df[min_df.combo_diff == min_df.combo_diff.min()]

    final_df = min_df.drop(['diff_from_115', 'combo_diff', 'all_diff'], 1)

    final_cols = [col[:-2] for col in list(final_df) if col.endswith('_x')]

    x_cols = [col for col in list(final_df) if col.endswith('_x')]
    x_df = final_df[x_cols]

    y_cols = [col for col in list(final_df) if col.endswith('_y')]
    y_df = final_df[y_cols]

    x_df.columns = final_cols
    y_df.columns = final_cols

    selected_options_df = x_df.append(y_df)
    option_types = set(selected_options_df['optionType'])
    if option_types == {'PE', 'CE'}:
        print('Both CALL and PUT options are selected for next week...')
    else:
        print('Check the select_final_options method in g1_fetch_week_options.py')
    print('Selected options below...')
    print(selected_options_df)
    return selected_options_df

def generate_final_options():
    df = read_input_df(ALL_OPTION_DATA_PATH)
    cleaned_df = clean_df(df)
    diff_col_df = add_diff_col_df(cleaned_df)
    final_df = final_data_df(diff_col_df)
    pd.set_option('display.max_rows', None)
    selected_df = select_final_options(final_df)
    write_option_data_to_csv(selected_df, SELECTED_WEEK_OPTIONS_PATH)
    write_option_data_to_csv(selected_df.drop('absolute_difference', 1), OPTION_LOG_PATH)
    write_option_data_to_csv(selected_df.drop('absolute_difference', 1), ADJUSTMENTS_PATH)


def selected_options_exist(path_string: str):
    selected_file = Path(path_string)
    if selected_file.is_file():
        return True
    else:
        return False

def log_current_option_data():
    current_df = current_option_data()
    print(f"Current option position below...")
    print(current_df)
    write_option_data_to_csv(current_df, OPTION_LOG_PATH, mode='a', header=False)

def process_option_data():
    if selected_options_exist(SELECTED_WEEK_OPTIONS_PATH):
        print('Selected options exist for current week...')
        print('Below are selected options for current week....')
        print(read_input_df(SELECTED_WEEK_OPTIONS_PATH))
        print("Logging current run's updated selected option data.... ")
        log_current_option_data()
    else:
        generate_final_options()

if __name__ == "__main__":
    process_option_data()

