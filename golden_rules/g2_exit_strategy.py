
current_df = current_option_data()
print(f"Current option position below...")
print(current_df)

selected_options_df = selected_week_options('DF')
check_premium_drop(selected_options_df, current_df)

def current_option_data():
    df = read_input_df(ALL_OPTION_DATA_PATH)
    selected_options_df = read_input_df(SELECTED_WEEK_OPTIONS_PATH)
    selected_options_dict = selected_options_df.set_index('optionType').T.to_dict('dict')
    call_strike_price = selected_options_dict['CE']['strikePrice']
    put_strike_price = selected_options_dict['PE']['strikePrice']
    current_df = df.loc[((df.optionType == "CE") & (df.strikePrice == call_strike_price)) |
                        (df.optionType == "PE") & (df.strikePrice == put_strike_price)]

    print(f"Current option position below...")
    print(current_df)
    check_premium_drop(selected_options_df, current_df)
    write_option_data_to_csv(current_df, OPTION_LOG_PATH, mode='a', header=False)
