from enum import Enum

PREMIUM = 115
PREMIUM_FALL_PERCENT = 75

ADJUSTMENT_RANGE = (80, 95)

ALL_OPTION_DATA_PATH  = 'data/next_week_options.csv'
SELECTED_WEEK_OPTIONS_PATH = 'data/selected_week_options.csv'
OPTION_LOG_PATH = 'data/selected_week_options_log.csv'
ADJUSTMENTS_PATH = 'data/adjusted_options.csv'

HEADERS = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36','Accept-Encoding': 'gzip, deflate, br','Accept-Language': 'en-US,en;q=0.9,hi;q=0.8'}

class Index(Enum):
    BANKNIFTY = 'BANKNIFTY'
    NIFTY = 'NIFTY'

CONFIG = {
    'expiry_date': '29-Jul-2021',
    'index_name': Index.BANKNIFTY.value,
    'short_strangle_with_buy': False
}

url =f"https://www.nseindia.com/api/option-chain-indices?symbol={CONFIG['index_name']}"

# 30 seconds or 5 minutes
OPTION_DATA_EXTRACTION_INTERVAL = 300